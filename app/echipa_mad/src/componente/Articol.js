import React from 'react';
import axios from 'axios';

export class Articol extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.titlu = "";
        this.state.autor  = "";
        this.state.publicatDe = "";
        this.state.descriereArticol = "";
        this.state.denumireCategorie = "";
    }
    
    
    handleChangeTitlu = (event) => {
        this.setState({
            titlu: event.target.value
        })
    }
    
    handleChangeAutor = (event) => {
        this.setState({
            autor: event.target.value
        })
    }
    
    handleChangePublicatDe = (event) => {
        this.setState({
            publicatDe: event.target.value
        })
    }
    
     handleChangeDescriereArticol = (event) => {
        this.setState({
            descriereArticol: event.target.value
        })
    }
    
     handleChangeDenumireCategorie = (event) => {
        this.setState({
            denumireCategorie: event.target.value
        })
    }
    
    handleAddClick = () => {
        let articol = {
            titlu: this.state.titlu,
            autor: this.state.autor,
            publicatDe: this.state.publicatDe,
            descriereArticol: this.state.descriereArticol,
            denumireCategorie: this.state.denumireCategorie
        }
        
        axios.post('https://proiect-final-mihaelatudor.c9users.io/creare-articol', articol).then((res) => {
            if(res.status === 200){
                this.props.articolAdaugat(articol)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
        
    render(){
        return(
           <div className="articolListMain">
            
            <form className = "articol" onSubmit={this.props.addArticol}>
            <label for="titlu">Titlu:</label>
            <input type="text" id="titlu" placeholder = "introduceti titlul" 
                   onChange={this.handleChangeTitlu}
                   value={this.state.titlu}/>
            
            <label for="autor">Autor:</label>
            <input type="text" id="autor" placeholder = "introduceti autorul"
                   onChange={this.handleChangeAutor}
                   value={this.state.autor}/>
            
            <label for="publicatie">Publicatie:</label>
            <input type="text" id="publicatie" placeholder = "introduceti publicatia"
                   onChange={this.handleChangePublicatDe}
                   value={this.state.publicatDe}/>
            
             <label for="descriere">Descriere:</label>
            <input type="text" id="descriere" placeholder = "introduceti descrierea"
                   onChange={this.handleChangeDescriereArticol}
                   value={this.state.descriereArticol}/>
            
             <label for="categorie">Categorie:</label>
            <input type="text" id="categorie" placeholder = "introduceti categoria"
                   onChange={this.handleChangeDenumireCategorie}
                   value={this.state.denumireCategorie}/>
            
            <button type="submit" id="articolBTN" onClick={this.handleAddClick}>Adauga articol</button>
            </form>
            
            </div>
            );
    }
}

export default Articol