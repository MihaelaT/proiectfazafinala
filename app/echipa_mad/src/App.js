import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Pagina from './componente/Pagina';
import Articol from './componente/Articol';

class App extends Component {
   constructor(props){
    super(props);
    this.state = {};
    this.state.articole = [];
  }
  
  onArticolAdded = (articol) => {
    let articole = this.state.articole;
    articole.push(articol);
    this.setState({
      articole: articole
    });
  }
  

  render() {
    return (
      <React.Fragment>
        <Pagina />
          
        <Articol articolAdaugat={this.onArticolAdded}/>
      
      </React.Fragment>
    );
  }
}

export default App;
