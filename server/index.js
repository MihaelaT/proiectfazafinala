const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const app = express();
app.use(bodyParser.json());

const sequelize = new Sequelize('c9', 'mihaelatudor', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize.authenticate().then(()=>{
    console.log('Conexiunea a fost stabilita cu succes!');
}).catch(err =>{
    console.error('Nu se poate efectua conexiunea cu baza de date: ',err);
});

const Utilizator = sequelize.define('utilizator', {
    nume: {
        type: Sequelize.STRING,
        allowNull: false
    },
    prenume: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: true
    },
    titluAcademic: {
        type: Sequelize.STRING,
        allowNull: false
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true
    },
    parola: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Articol = sequelize.define('articol', {
    codArticol: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    titlu: {
        type: Sequelize.STRING,
        allowNull: false
    },
    autor: {
        type: Sequelize.STRING,
        allowNull: false
    },
     publicatDe: {
        type: Sequelize.STRING,
        allowNull: true
    },
     descriereArticol: {
        type: Sequelize.STRING,
        allowNull: true
    },
     nrAccesari: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    citit: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    denumireCategorie: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Librarie = sequelize.define('librarie', {
    codLibrarie: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    codArticol: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    ultimaPaginaCitita: {
        type: Sequelize.STRING,
        allowNull: false
    },
    ratingArticol: {
         type: Sequelize.INTEGER,
        allowNull: false
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Categorie = sequelize.define('categorie', {
    codCategorie: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    denumireCategorie: {
        type: Sequelize.STRING,
        allowNull: false
    },
    descriereCategorie: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

Articol.belongsTo(Librarie);
Articol.belongsTo(Categorie);
Librarie.belongsTo(Utilizator);


sequelize.sync({force: true}).then(()=>{
    console.log('Baza de date a fost creata cu succes!')
});

app.get('/', (req, res) => {
    res.status(200).send('Faza 2 - echipa MAD');
})

app.post('/inregistrare', (req, res) => {
    Utilizator.create({
        nume: req.body.nume,
        prenume: req.body.prenume,
        email: req.body.email,
        titluAcademic: req.body.titluAcademic,
        username: req.body.username,
        parola: req.body.parola
    }).then((utilizator) => {
        res.status(200).send("Utilizatorul a fost creat cu succes!");
    }, (err) => {
        res.status(500).send(err);
    });
});

app.post('/logare', (req, res) => {
    Utilizator.findOne({where: {username: req.body.username, parola: req.body.parola}}).then((result)=> {
        res.status(200).send(result);
    });
});

app.post('/creare-articol', (req, res) => {
    Articol.create({
        titlu: req.body.titlu,
        autor: req.body.autor,
        publicatDe: req.body.publicatDe,
        descriereArticol: req.body.descriereArticol,
        nrAccesari: 0,
        citit: false,
        denumireCategorie: req.body.denumireCategorie
        
    }).then((articol)=>{
        res.status(200).send("Articolul a fost creat cu succes!");
    }, (err)=>{
       res.status(500).send(err);
    });
});

app.get('/get-articole', (req, res) =>{
    Articol.findAll().then((articole) => {
        res.status(200).send(articole);
    })
})

app.get('/get-articole-dupa-autor', (req, res) =>{
    Articol.findAll({
        where: {
            autor: req.body.autor
        }
    }).then((articole)=>{
        res.status(200).send(articole);
    });
});

app.put('/update-articol', (req,res)=>{
    const cod = req.body.codArticol;
    
    Articol.update({
        autor:req.body.autor,
        titlu:req.body.titlu,
        descriereArticol:req.body.descriereArticol
    }, {
        where:{
            codArticol: cod
        }
    }).then((articol) => {
         res.status(200).send("Articolul a fost modificat cu succes!");
    }, (err)=>{
         res.status(404).send('Articolul cu codul: ${cod} nu exista!')
    });
});

app.delete('/delete-articol-dupa-titlu', (req,res)=>{
    Articol.destroy({
        where: {
            titlu: req.body.titlu
        }
    }).then((articol)=>{
        res.status(200).send(`Articolul cu titlul: ${req.body.titlu} a fost sters!`);
    }, (err) => {
        res.status(404).send('Articolul cu titlul: ${req.body.titlu} nu exista!')
    })
})

app.get('/get-articol-dupa-categorie', (req, res)=>{
      Articol.findAndCountAll({
        where: {
            denumireCategorie: req.body.denumireCategorie
        }
    }).then((articole)=>{
        res.status(200).send(articole);
    });
})

app.delete('/delete-articole-dupa-categorie',(req,res)=>{
    Articol.destroy({
        where: {
            denumireCategorie: req.body.denumireCategorie
        }
    }).then((articol)=>{
        res.status(200).send(`Articolele din categoria: ${req.body.denumireCategorie} au fost sterse!`);
    }, (err) => {
        res.status(404).send('Categoria: ${req.body.denumireCategorie} nu exista!')
    })
})

app.post('/creare-categorie', (req, res) => {
   Categorie.create({
       denumireCategorie: req.body.denumireCategorie,
       descriereCategorie: req.body.descriereCategorie
    }).then((categorie)=>{
        res.status(200).send("Categoria a fost creat cu succes!");
    }, (err)=>{
       res.status(500).send(err);
    });
});

app.get('/get-categorii', (req, res) =>{
    Categorie.findAll().then((categorii) => {
        res.status(200).send(categorii);
    })
})

app.post('/creare-librarie', (req, res) => {
   Librarie.create({
       username: req.body.username,
       codArticol: req.body.codArticol,
       ultimaPaginaCitita: req.body.ultimaPaginaCitita,
       ratingArticol: req.body.ratingArticol
    }).then((librarie)=>{
        res.status(200).send("Libraria a fost creat cu succes!");
    }, (err)=>{
       res.status(500).send(err);
    });
});

app.get('/get-librarie', (req, res) =>{
    Librarie.findAll().then((librarie) => {
        res.status(200).send(librarie);
    })
})

app.put('/update-librarie-dupa-codArticol-si-username', (req,res)=>{
    Librarie.update({
       ultimaPaginaCitita: sequelize.literal('ultimaPaginaCitita + 1')
    }, {
        where:{
            codArticol: req.body.codArticol,
            username: req.body.username
        }
    }).then((articol) => {
         res.status(200).send("Articolul din librarie a fost modificat cu succes!");
    }, (err)=>{
         res.status(404).send(`Articolul cu codul: ${req.body.codArticol} nu exista!`)
    });
});

app.delete('/delete-categorie-fara-articole-dupa-denumire', (req,res)=>{
   Articol.findAndCountAll({
       where:{
           denumireCategorie: req.body.denumire
       }
   }).then(articole =>{
       if (articole.count === 0)
       {
           Categorie.destroy({
               where: {
                   denumireCategorie: req.body.denumireCategorie
               }
           });
           res.status(200).send("Categoria a fost stearsa cu succes!");
       }
       else
       {
           res.status(200).send("Categoria nu poate fi stearsa deoarece exista cel putin un articol care face parte din ea!");
       }
   });
});


app.listen(8080, ()=>{
    console.log('Server started on port 8080 :)');
})